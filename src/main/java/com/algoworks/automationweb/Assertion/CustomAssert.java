package com.algoworks.automationweb.Assertion;

import org.testng.Assert;
import org.testng.Reporter;
import com.google.common.base.Throwables;

public class CustomAssert
{
	private static StringBuilder verificationErrors = new StringBuilder();
	private Throwable t = null;

	private static void clearVerificationErrors()
	{
		verificationErrors = new StringBuilder();
	}

	public void fail(String s)
	{
		try
		{
			Assert.fail(s);
		}
		catch (AssertionError e)
		{
			t = e;
			verificationErrors.append(e);
			verificationErrors.append("\n");
			Reporter.log(s + "");
		}
	}

	public void fail(Throwable e)
	{
		try
		{
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
		catch (AssertionError m)
		{
			t = e;
			verificationErrors.append(Throwables.getStackTraceAsString(e) + "\n\n");
			Reporter.log(Throwables.getStackTraceAsString(e));
			Reporter.log("\n\n");
		}
	}

	public void failonFatalError(String s) // this will be used for checking errors in URLs
	{
		try
		{
			Assert.fail(s);
		}
		catch (AssertionError m)
		{
			verificationErrors.append(m);
			Reporter.log(Throwables.getStackTraceAsString(m));
			verificationErrors.append("\n");
		}
	}

	public Boolean assertTrue(boolean actualCondition)
	{
		Boolean flag = false;
		try
		{
			Assert.assertTrue(actualCondition);
			flag = true;
		}
		catch (AssertionError e)
		{
			t = e;
			verificationErrors.append(e);
			verificationErrors.append("\n");
			Reporter.log(Throwables.getStackTraceAsString(e) + "");
		}
		return flag;
	}

	public Boolean assertFalse(boolean actualCondition)
	{
		Boolean flag = false;
		try
		{
			Assert.assertFalse(actualCondition);
			flag = true;
		}
		catch (AssertionError e)
		{
			t = e;
			verificationErrors.append(e);
			verificationErrors.append("\n");
			Reporter.log(Throwables.getStackTraceAsString(e) + "");
		}
		return flag;
	}

	public Boolean assertEquals(Object actual, Object expected)
	{
		Boolean isEqual = false;
		try
		{
			Assert.assertEquals(actual, expected);
			isEqual = true;
		}
		catch (AssertionError e)
		{
			t = e;
			verificationErrors.append(e);
			verificationErrors.append("\n");
			Reporter.log(Throwables.getStackTraceAsString(e) + "");
		}
		return isEqual;
	}

	public Boolean assertContains(Object actual, Object expected)
	{
		Boolean isEqual = false;
		try
		{
			Assert.assertTrue(actual.toString().contains(expected.toString()));
			isEqual = true;
		}
		catch (AssertionError e)
		{
			verificationErrors.append(e);
			verificationErrors.append("\n");
			Reporter.log(Throwables.getStackTraceAsString(e) + "");
		}
		return isEqual;
	}

	/* This method will be used in the END before the test ends to display the errors in console */

	public void displayVerificationErrors()
	{
		String verificationMessage = verificationErrors.toString();
		clearVerificationErrors();
		if (!"".equalsIgnoreCase(verificationMessage))
		{
			Assert.fail(verificationMessage);
		}
	}

	public Throwable getThrowable()
	{
		return t;
	}
}
