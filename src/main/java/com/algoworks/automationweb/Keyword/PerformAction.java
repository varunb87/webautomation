package com.algoworks.automationweb.Keyword;

import java.awt.AWTException;
import java.awt.Robot;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.algoworks.automationweb.Assertion.CustomAssert;
import com.algoworks.automationweb.Drivers.BaseDriver;
import com.algoworks.automationweb.Drivers.MainDriver;
import com.algoworks.automationweb.Reporting.ExtentTesting;
import com.algoworks.automationweb.Utility.CreateLocator;
import com.algoworks.automationweb.Utility.CustomWait;
import com.algoworks.automationweb.Utility.ImageResponse;
import com.algoworks.automationweb.Utility.TestNGParameters;
import com.algoworks.automationweb.Utility.Utils;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;

public class PerformAction
{
	private ExtentTesting er = null;
	private CreateLocator createLoc = null;
	private RemoteWebDriver driver = null;
	private CustomWait customWait = null;
	private CustomAssert customAssert = null;
	private final int MAX_ATTEMPTS = 30;
	private String failMsg = null;
	private static final Logger log = LogManager.getLogger(PerformAction.class.getName());
	private String ordernumber;
	private Set<String> windows;
	private String currentWindow;
	private List<Double> total = null;
	private TestNGParameters parameters;

	public PerformAction(TestNGParameters parameters, CustomAssert customAssert)
	{
		this.parameters = parameters;
		driver = BaseDriver.getDriver();
		currentWindow = driver.getWindowHandle();
		createLoc = new CreateLocator();
		er = MainDriver.getExtentTestObject();
		customWait = new CustomWait(driver);
		this.customAssert = customAssert;
		total = new ArrayList<>();
	}

	public void perform(String action, String element, String category, String expectedData) throws InterruptedException, AWTException, IOException
	{
		By locator = null;
		int attempts = 0;
		String actualMessage = null;
		Boolean clickFail = false;
		Boolean hasText = false;
		boolean isVisible = false;
		element = element.trim();
		String fieldLabel = null;
		String expectedPrice = null;
		String actualPrice = null;
		if ((!isTextEmpty(element) && !isTextEmpty(category)) || (!("BLANK").trim().equalsIgnoreCase(element) && !("BLANK").trim().equalsIgnoreCase(category)))
		{
			locator = createLoc.getByLocator(element, category);
		}
		switch (action.toUpperCase())
		{
			case "ADDPRICE":
				expectedPrice = driver.findElement(locator).getText();
				if (expectedPrice.contains("$"))
				{
					expectedPrice = expectedPrice.replace("$", "");
				}
				double price_equivalent = Double.valueOf(expectedPrice);
				total.add(price_equivalent);
				break;

			case "COMPARETOTAL":
				Iterator<Double> itr = total.iterator();
				double sum = 0;
				while (itr.hasNext())
				{
					sum = sum + itr.next();
				}
				expectedPrice = "$" + String.valueOf(sum);
				actualPrice = driver.findElement(locator).getText();
				if (customAssert.assertEquals(actualPrice, expectedPrice))
				{
					er.passTest("Actual price matched with the expected price");
				}
				else
				{
					failMsg = "Actual = " + actualPrice + " but expected = " + expectedPrice;
					Assert.fail(failMsg);
				}

				break;

			case "ASSERT_DISABLED":
				customWait.explicitWait(locator);
				String disabledVal = checkDisabled(locator);
				if (disabledVal.equalsIgnoreCase("true"))
				{
					er.passTest("\"" + fieldLabel + "\"" + " is disabled as expected.");
				}
				else
				{
					failMsg = "\"" + fieldLabel + "\"" + " is enabled. This is unexpected!";
					Assert.fail(failMsg);
				}
				break;

			case "ASSERT_HIDDEN":
				fieldLabel = locator.toString();
				customWait.waitForElementToHide(locator);
				er.passTest("\"" + fieldLabel + "\"" + " is hidden as expected.");
				break;

			case "ASSERT_VISIBLE":
				customWait.explicitWait(locator);
				attempts = 0;
				while (attempts < MAX_ATTEMPTS)
				{
					try
					{
						fieldLabel = getLabel(element, locator);
						if (element.equalsIgnoreCase(fieldLabel))
						{
							er.passTest("\"" + fieldLabel + "\"" + " is visible on the web page.");
						}
						else
						{
							er.passTest(element + " is visible on the web page. The text says " + "\"" + fieldLabel + "\"");
						}
						break;
					}
					catch (StaleElementReferenceException e)
					{
						log.debug(e + " was encountered. Ignoring....");
					}
					++attempts;
				}
				break;

			case "CLICK":
				// customWait.waitForElement(driver, locator);
				customWait.explicitWait(locator);
				isVisible = false;
				while (!isVisible && attempts < MAX_ATTEMPTS)
				{
					Thread.sleep(500);
					try
					{
						fieldLabel = getLabel(element, locator);
						driver.findElement(locator).click();
						er.passTest("\"" + fieldLabel + "\"" + " was clicked.");
						isVisible = true;
					}
					catch (WebDriverException e)
					{
						++attempts;
					}
				}

				if (!isVisible && attempts >= MAX_ATTEMPTS)
				{
					failMsg = fieldLabel + " is not visible & could not be clicked!";
					Assert.fail(failMsg);
				}
				break;

			case "CLICKATBLANK":
				customWait.explicitWait(locator);
				driver.findElement(By.tagName("body")).click();
				break;

			case "CHECKITEMS":
				customWait.waitForElement(locator, 30);
				String numOfItems = driver.findElement(locator).getText().trim();
				attempts = 0;
				while (attempts < 15)
				{
					if (!"0".equalsIgnoreCase(numOfItems))
					{
						er.passTest("Item was added successfully.");
						break;
					}
					else
					{
						++attempts;
					}
				}
				if (attempts >= 15)
				{
					failMsg = "Item was not added!";
					Assert.fail(failMsg);
				}
				break;

			case "CHECKPRICE":
				customWait.explicitWait(locator);
				actualPrice = driver.findElement(locator).getText().trim();
				if (!("$0.00".equalsIgnoreCase(actualPrice)) && !("$0.0".equalsIgnoreCase(actualPrice)) && !("0".equalsIgnoreCase(actualPrice)))
				{
					er.passTest("The item/quantity/amount is greater than 0. Actual  = " + actualPrice);
				}
				else
				{
					failMsg = "0 or $0.00 was found! This was not expected!";
					Assert.fail(failMsg);
				}
				break;

			case "CLOSE_POPUP":
				customWait.addImplicitWait(2);
				driver.findElement(locator).click();
				customWait.removeImplicitWait();
				er.passTest("The popup was closed.");
				break;

			case "GO BACK":
				driver.navigate().back();
				er.passTest("Previous page was revisited.");
				break;

			case "HOVER":
				customWait.addImplicitWait(30);
				attempts = 0;
				while (attempts <= MAX_ATTEMPTS)
				{
					Thread.sleep(100);
					try
					{
						fieldLabel = getLabel(element, locator);
						driver.findElement(locator).click();
						er.passTest("\"" + fieldLabel + "\"" + " was clicked.");
						break;
					}
					catch (WebDriverException | NullPointerException e)
					{
						log.debug(e + " was encountered. Ignoring....");
					}
					++attempts;
				}
				if (attempts > MAX_ATTEMPTS)
				{
					failMsg = "\"" + fieldLabel + "\"" + " was not clicked. Element was not visible!";
					Assert.fail(failMsg);
				}
				customWait.removeImplicitWait();
				Thread.sleep(500);
				break;

			case "HOVER 2":
				/* JavascriptExecutor js = (JavascriptExecutor) driver; js.
				 * executeAsyncScript("if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover',"
				 * + "true, false); arguments[0].dispatchEvent(evObj);} else if(document.createEventObject)" +
				 * "{ arguments[0].fireEvent('onmouseover');}", driver.findElement(locator)); */
				Robot robot = new Robot();
				Point point = driver.findElement(locator).getLocation();
				robot.mouseMove(point.getX() + 20, point.getY() + 110);
				fieldLabel = getLabel(element, locator);
				er.passTest("The menu " + "\"" + fieldLabel + "\"" + " was found & mouse was hovered.");
				Thread.sleep(500);
				break;

			case "REFRESH":
				driver.navigate().refresh();
				customWait.addImplicitWait(2);
				attempts = 0;
				while (attempts <= 30 && !isVisible)
				{
					try
					{
						driver.findElement(locator);
						isVisible = true;
						er.passTest("Page was refreshed.");
						customWait.removeImplicitWait();
						break;
					}
					catch (StaleElementReferenceException | NullPointerException | NoSuchElementException e)
					{
						attempts++;
						log.debug(e + " was encountered. Ignoring....");
					}
				}
				break;

			case "SCROLLUP":
				driver.executeScript("window.scrollTo(0, 0)");
				break;

			case "SCROLLDOWN":
				driver.executeScript("window.scrollTo(0, document.body.scrollHeight)");
				break;

			case "SCROLLTO":
				driver.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(locator));
				Thread.sleep(300);
				break;

			case "SELECT":
				customWait.waitForElement(locator, 30);
				isVisible = false;
				attempts = 0;
				while (!isVisible && attempts < MAX_ATTEMPTS)
				{
					try
					{
						fieldLabel = getLabel(element, locator);
						new Select(driver.findElement(locator)).selectByVisibleText(expectedData);
						er.passTest("\"" + expectedData + "\"" + " was selected in the drop down: " + fieldLabel);
						isVisible = true;
					}
					catch (StaleElementReferenceException e)
					{
						log.debug(e + " was encountered. Ignoring....");
						++attempts;
					}
				}
				if (attempts >= MAX_ATTEMPTS && !isVisible)
				{
					fieldLabel = locator.toString();
					failMsg = fieldLabel + " could not be selected!";
					Assert.fail(failMsg);
				}
				break;

			case "SELECTFRAME":
				customWait.explicitWait(locator);
				WebElement frame = driver.findElement(locator);
				driver.switchTo().frame(frame);
				break;

			case "STOREORDER":
				customWait.waitForElement(locator, 30);
				ordernumber = driver.findElement(locator).getText();
				break;

			case "SWITCH TO NEW WINDOW":
				windows = driver.getWindowHandles();
				for (String s : windows)
				{
					if (!s.equals(currentWindow))
					{
						driver.switchTo().window(s);
						break;
					}
				}
				break;

			case "SWITCH TO OLD WINDOW":
				driver.switchTo().window(currentWindow);
				break;

			case "TYPE":
				customWait.explicitWait(locator);
				attempts = 0;
				clickFail = false;
				while (attempts <= MAX_ATTEMPTS && !clickFail)
				{
					try
					{
						fieldLabel = getLabel(element, locator);
						driver.findElement(locator).clear();
						driver.findElement(locator).sendKeys(expectedData);
						er.passTest("\"" + expectedData + "\"" + " was typed successfully in the " + fieldLabel + "  textbox.");
						clickFail = true;
					}
					catch (InvalidElementStateException | StaleElementReferenceException e)
					{
						++attempts;
						log.debug(e + " was encountered. Ignoring so that the field can load properly.", e);
					}
				}

				if (attempts > MAX_ATTEMPTS && !clickFail)
				{
					failMsg = "NoSuchElementException! " + element + " did not load properly even after " + attempts + " attempts!";
					throw new NoSuchElementException(failMsg);
				}
				break;

			case "TYPEINIFRAME":
				fieldLabel = getLabel(element, locator);
				driver.findElement(locator).clear();
				driver.findElement(locator).sendKeys(expectedData);
				er.passTest("\"" + expectedData + "\"" + " was typed successfully in the " + fieldLabel + "  textbox.");
				driver.switchTo().defaultContent();
				break;

			/* case "UPDATECELL": UpdateExcel update = new UpdateExcel(); update.updateCell(Integer.parseInt(element),
			 * Integer.parseInt(expectedData)); break; */

			case "UPLOAD":
				customWait.explicitWait(locator);
				if (driver.findElement(locator).isDisplayed())
				{
					driver.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(locator));
					driver.findElement(locator).sendKeys(expectedData);
					er.passTest("\"" + expectedData + "\"" + " was uploaded.");
					Thread.sleep(100);
				}
				else
				{
					failMsg = "Upload did not work. Element is not visible.";
					Assert.fail(failMsg);
				}
				break;

			case "VERIFY":
				// this is for testing values in textboxes
				customWait.explicitWait(locator);
				Boolean isEqual = false;
				isEqual = customAssert.assertEquals(driver.findElement(locator).getAttribute("value"), expectedData);
				if (isEqual)
					er.passTest("Expected & actual values matched.");
				else
				{
					failMsg = "Actual = " + driver.findElement(locator).getAttribute("value") + " and Expected = " + "\"" + expectedData + "\"";
					Assert.fail(failMsg);
				}
				break;

			case "VERIFYDROPDOWN":
				customWait.explicitWait(locator);
				Boolean isDropDownValueEqual = false;
				String currentDropdownVal = new Select(driver.findElement(locator)).getFirstSelectedOption().getText();
				isDropDownValueEqual = customAssert.assertEquals(currentDropdownVal, expectedData);
				if (isDropDownValueEqual)
				{
					er.passTest("Current drop down value matched with the expected. Data = " + expectedData);
				}
				else
				{
					failMsg = "Values do not match. Expected = " + expectedData + " and Actual = " + currentDropdownVal;
					Assert.fail(failMsg);
				}
				break;

			case "VERIFYERROR":
				try
				{
					customWait.waitForElement(locator, 10);
					driver.findElement(locator).isDisplayed();
					failMsg = driver.findElement(locator).getText();
					if (Utils.isTextNotBlankandEmpty(failMsg))
					{
						Assert.fail(failMsg);
					}
				}
				catch (TimeoutException e)
				{
					log.debug(e + " was encountered. This is expected....");
					er.passTest("No error was found.");
				}
				break;

			case "VERIFYIMAGE":
				customWait.explicitWait(locator);
				ImageResponse response = new ImageResponse();
				WebElement image = driver.findElement(locator);
				int responseCode = 0;
				responseCode = response.getImageResponse(driver, locator);
				if (responseCode == 200)
				{
					er.passTest("Image found at " + image.getAttribute("src"));
				}
				else
				{
					failMsg = "Image = " + image.getAttribute("src") + "is broken. Error code = " + responseCode;
					Assert.fail(failMsg);
				}
				break;

			case "VERIFYMESSAGE":
				// this is for testing messages
				customWait.explicitWait(locator);
				actualMessage = null;
				attempts = 0;
				hasText = false;
				while (attempts <= 15 && !hasText)
				{
					try
					{
						if (Utils.isTextNotBlankandEmpty(driver.findElement(locator).getText().trim()))
						{
							actualMessage = driver.findElement(locator).getText().trim();
							hasText = true;
						}
						else if (Utils.isTextNotBlankandEmpty(driver.findElement(locator).getAttribute("value").trim()))
						{
							actualMessage = driver.findElement(locator).getAttribute("value").trim();
							hasText = true;
						}
						else
						{
							++attempts;
						}
					}
					catch (StaleElementReferenceException e)
					{
						++attempts;
						log.debug(e + " was encountered. Ignoring.");
					}
				}
				expectedData = expectedData.trim();
				if (customAssert.assertEquals(actualMessage, expectedData))
				{
					er.passTest("\"" + expectedData + "\"" + " message was found.");
				}
				else
				{
					failMsg = "Expected " + "\"" + expectedData + "\"" + " but found " + "\"" + actualMessage + "\"";
					Assert.fail(failMsg);
				}
				break;

			case "VERIFYORDER":
				if (customAssert.assertEquals(driver.findElement(locator).getText(), ordernumber))
				{
					er.passTest("Order number: " + ordernumber + " was found.");
				}
				else
				{
					failMsg = "Order numbers did not match!";
					Assert.fail(failMsg);
				}
				break;

			case "VERIFYPROGRESS":
				customWait.explicitWait(locator, expectedData);
				er.passTest("\"" + expectedData + "\"" + " was found. Progress successful.");
				break;

			case "VERIFYTEXTCONTAIN":
				customWait.explicitWait(locator);
				actualMessage = null;
				attempts = 0;
				hasText = false;
				while (attempts <= 15 && !hasText)
				{
					try
					{
						if (Utils.isTextNotBlankandEmpty(driver.findElement(locator).getText()))
						{
							actualMessage = driver.findElement(locator).getText();
							hasText = true;
						}
						else if (Utils.isTextNotBlankandEmpty(driver.findElement(locator).getAttribute("value")))
						{
							actualMessage = driver.findElement(locator).getAttribute("value");
							hasText = true;
						}
						else
						{
							++attempts;
						}
					}
					catch (StaleElementReferenceException e)
					{
						++attempts;
						log.debug(e + " was encountered. Ignoring.");
					}
				}
				if (attempts <= 15)
				{
					expectedData = expectedData.trim();
					if (customAssert.assertContains(actualMessage, expectedData))
					{
						er.passTest("\"" + expectedData + "\"" + " message was found.");
					}
					else
					{
						failMsg = "Expected " + "\"" + expectedData + "\"" + " to be present in the text but found " + "\"" + actualMessage + "\"";
						Assert.fail(failMsg);
					}
				}
				else
				{
					failMsg = "ElementNotVisibleException! " + element + " was not visible!";
					throw new ElementNotVisibleException(failMsg);
				}
				break;

			case "VERIFYTITLE":
				if (customAssert.assertEquals(driver.getTitle(), expectedData))
				{
					er.passTest("Title of the current window matched with the expected title.");
				}
				else if (customAssert.assertContains(driver.getTitle(), expectedData))
				{
					er.passTest("Title of the current window matched with the expected title.");
				}
				else
				{
					failMsg = "Title of the current window did not match with the expected title.";
					Assert.fail(failMsg);
				}
				break;
				
			case "VERIFYURL":
				if (customAssert.assertEquals(driver.getCurrentUrl(), expectedData))
				{
					er.passTest("Actual URL matched with the expected URL.");
				}
				else
				{
					failMsg = "Actual URL does not match the expected URL. Actual URL = " + driver.getCurrentUrl();
					Assert.fail(failMsg);
				}
				break;

			case "VISIT":
				String url = parameters.getBaseurl();
				if (Utils.isTextNotBlankandEmpty(url))
				{
					driver.get(url);
					er.passTest("The url " + "\"" + url + "\"" + " was visited.");
				}
				else
				{
					driver.get(expectedData);
					er.passTest("The url " + "\"" + expectedData + "\"" + " was visited.");
				}
				break;

			case "WAIT":
				Thread.sleep(5000);
				break;

			default:
				throw new NullPointerException("This is an invalid action!!");
		}
	}

	private boolean isTextEmpty(String actualMessage)
	{
		return StringUtils.isBlank(actualMessage) || StringUtils.isEmpty(actualMessage);
	}

	private String checkDisabled(By locator)
	{
		return driver.findElement(locator).getAttribute("disabled");
	}

	private String getLabel(String element, By locator)
	{
		String fieldlabel = "";
		while (StringUtils.isEmpty(fieldlabel) && StringUtils.isBlank(fieldlabel))
		{
			if (StringUtils.isNotBlank(driver.findElement(locator).getAttribute("title")) && StringUtils.isNotEmpty(driver.findElement(locator).getAttribute("title")) && !"0".equalsIgnoreCase(driver.findElement(locator).getAttribute("title")))
			{
				fieldlabel = driver.findElement(locator).getAttribute("title");
			}
			else if (StringUtils.isNotBlank(driver.findElement(locator).getAttribute("value")) && StringUtils.isNotEmpty(driver.findElement(locator).getAttribute("value")) && !"0".equalsIgnoreCase(driver.findElement(locator).getAttribute("value")))
			{
				fieldlabel = driver.findElement(locator).getAttribute("value");
			}
			else if (StringUtils.isNotBlank(driver.findElement(locator).getText()) && StringUtils.isNotEmpty(driver.findElement(locator).getText()) && !"0".equalsIgnoreCase(driver.findElement(locator).getText()))
			{
				fieldlabel = driver.findElement(locator).getText();
			}
			else
			{
				fieldlabel = element;
			}
		}
		return fieldlabel;
	}

	public String getFailMessage()
	{
		return failMsg;
	}
}
