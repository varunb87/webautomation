package com.algoworks.automationweb.ListenerInfo;

import org.apache.commons.lang3.StringUtils;
import org.testng.ITestContext;
import org.testng.TestListenerAdapter;
import com.algoworks.automationweb.Utility.Utils;

public class AutomationListener extends TestListenerAdapter
{
	private static final String BUILD_TAG = "BUILD_TAG";
	private static final String BUILD_NUMBER = "BUILD_NUMBER";

	@Override
	public void onStart(ITestContext testContext)
	{
		super.onStart(testContext);
		String buildTag = Utils.getEnvProperty(BUILD_TAG);
		if (StringUtils.isNotBlank(buildTag) && StringUtils.isNotEmpty(buildTag))
		{
			System.setProperty("buildTag", buildTag);
		}
		else
		{
			buildTag = Utils.convertTimestamptoText();
			System.setProperty("buildTag", buildTag);
		}

		String buildNumber = Utils.getEnvProperty(BUILD_NUMBER);
		if (StringUtils.isNotBlank(buildNumber) && StringUtils.isNotEmpty(buildNumber))
		{
			System.setProperty("buildNumber", buildNumber);
		}
		else
		{
			buildNumber = Utils.convertTimestamptoText();
			System.setProperty("buildNumber", buildNumber);
		}
	}
}
