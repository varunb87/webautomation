package com.algoworks.automationweb.Errors;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import com.algoworks.automationweb.Assertion.CustomAssert;
import com.algoworks.automationweb.Drivers.BaseDriver;
import com.algoworks.automationweb.Drivers.MainDriver;
import com.algoworks.automationweb.Reporting.ExtentTesting;
import com.google.common.base.Throwables;

public class ErrorReporting
{
	private RemoteWebDriver driver = null;
	private ExtentTesting testing = null;
	private CustomAssert customAssert = null;
	private static final Logger log = LogManager.getLogger(ErrorReporting.class.getName());
	private Screenshot screenshot = null;

	public ErrorReporting(RemoteWebDriver driver)
	{
		this.driver = driver;
		customAssert = new CustomAssert();
		testing = MainDriver.getExtentTestObject();
		screenshot  = new Screenshot(this.driver);
	}

	public void logErrors(Throwable e, String testCaseSheet, List<String> dataErrors, String failMsg) throws IOException
	{
		String testName = BaseDriver.getTestName();
		System.out.println();
		log.info("Test name having error = " + testName);
		log.info("Here are the errors: ");
		log.info("Sheet name: " + testCaseSheet);
		Iterator<String> itr = dataErrors.iterator();
		log.info("Action = " + itr.next());
		String element = itr.next();
		log.info("Element = " + element);
		String value = itr.next();
		if (!StringUtils.isEmpty(value) || !StringUtils.isBlank(value))
			log.info("Value = " + value);
		String rowNumber = itr.next();
		int row = Integer.parseInt(rowNumber) + 1;
		log.info("Excel sheet row number that has error = " + row);
		dataErrors.clear();
		String finalScreenshotPath = screenshot.takeScreenshot(testName, testCaseSheet, row);
		log.info("Screenshot saved at = " + finalScreenshotPath);
		
		if (e instanceof TimeoutException || e instanceof ElementNotVisibleException || e instanceof NoSuchElementException)
		{
			testing.markFatal(failMsg, finalScreenshotPath);  //log the error in the HTML report
			log.error(failMsg, e);
			//Assert.fail(Throwables.getStackTraceAsString(e));
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
		else if (e instanceof AssertionError)
		{
			log.error(failMsg);
			testing.failTest(failMsg, finalScreenshotPath);
			customAssert.fail(failMsg);
		}
		else
		{
			failMsg = "Unexpected error found at row number " + row + "\n\n" + e.getMessage();
			testing.markFatal(failMsg, finalScreenshotPath);
			log.fatal(failMsg, e);
			Assert.fail(Throwables.getStackTraceAsString(e)); // this is to ensure that no further tests are executed after this
			// customAssert.fail(e); /* Uncomment this & comment the above if further test steps are to be executed */
		}
	}
}
