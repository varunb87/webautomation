package com.algoworks.automationweb.Errors;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.ScreenshotException;

import com.algoworks.automationweb.Utility.JenkinsBuildPath;

public class Screenshot
{
	private RemoteWebDriver driver = null;
	public Screenshot(RemoteWebDriver driver)
	{
		this.driver = driver;
	}

	// Print screenshot like TestName_sheetName_rownumber.png
	public String takeScreenshot(String testname, String testCaseSheet, int row)
	{
		String finalScreenshotPath = null;
		File scrFile = null;
		try
		{
			scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			String screenshotFile = testname + "_" + testCaseSheet + "_" + "build_" + System.getProperty("buildNumber") + "_row" + row + ".png";
			String slash = null;
			if (SystemUtils.IS_OS_MAC_OSX || SystemUtils.IS_OS_LINUX)
			{
				slash = "/";
			}
			else if (SystemUtils.IS_OS_WINDOWS)
			{
				slash = "\\";
			}
			finalScreenshotPath = (new File(JenkinsBuildPath.getBuildPath()).getAbsolutePath() + slash
					+ screenshotFile);

			FileUtils.copyFile(scrFile, new File(finalScreenshotPath));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch(ScreenshotException e)
		{
			//do nothing
		}

		if (SystemUtils.IS_OS_MAC_OSX || SystemUtils.IS_OS_LINUX)
		{
			finalScreenshotPath = "file://" + finalScreenshotPath;
		}
		else if (SystemUtils.IS_OS_WINDOWS)
		{
			finalScreenshotPath = "file:\\" + finalScreenshotPath;
		}
		return finalScreenshotPath;
	}

}
