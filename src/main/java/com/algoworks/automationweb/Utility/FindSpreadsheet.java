package com.algoworks.automationweb.Utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import com.algoworks.automationweb.Utility.ReadExcel;

public class FindSpreadsheet
{
	public Object[][] getArray(String fileName, String testCaseSheet) throws InvalidFormatException, IOException
	{
		Object[][] testStep = null;
		ReadExcel read = new ReadExcel();
		testStep = read.returnArray(fileName, testCaseSheet);

		/* Now remove null rows from the current sheet that were introduced by tools like 
		 * Google Sheets or Libreoffice */
		List<List<Object>> temp = new ArrayList<>();
		int listindex = 0;
		for (int i = 0; i < testStep.length; i++)
		{
			if (ArrayUtils.isNotEmpty(testStep[i]) && testStep[i] != null)
			{
				temp.add(listindex, new ArrayList<>());
				for (int j = 0; j < testStep[i].length; j++)
				{
					if (testStep[i][j] != null)
					{
						temp.get(i).add(testStep[i][j]);
					}
				}
				++listindex;
			}
		}

		/* Convert multi dimension ArrayList to 2D array */
		ArrayActions actions = new ArrayActions();
		Object[][] finalarray = actions.getArrayfromList(temp);
		return finalarray;
	}

	public String[][] getStringArrayfromObject(Object[][] array)
	{
		String[][] finalarray = new String[array.length][];
		for (int i = 0; i < array.length; i++)
		{
			finalarray[i] = Arrays.stream(array[i]).toArray(String[]::new);
		}
		return finalarray;
	}
}
