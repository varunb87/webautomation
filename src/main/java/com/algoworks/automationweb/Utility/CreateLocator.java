package com.algoworks.automationweb.Utility;

import org.openqa.selenium.By;

public class CreateLocator
{
	private By locator = null;

	public By getByLocator(String element, String category)
	{
		switch (category.toLowerCase())
		{
			case "id":
				locator = By.id(element);
				break;

			case "css":
				locator = By.cssSelector(element);
				break;

			case "class":
				locator = By.className(element);
				break;

			case "link text":
				locator = By.linkText(element);
				break;

			case "name":
				locator = By.name(element);
				break;

			case "partial link text":
				locator = By.partialLinkText(element);
				break;

			case "xpath":
				locator = By.xpath(element);
				break;
				
			default:
				locator = By.xpath(element);
		}
		return locator;
	}

}
