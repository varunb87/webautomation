package com.algoworks.automationweb.Utility;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ImageResponse
{
	public int getImageResponse(RemoteWebDriver driver, By locator) throws InterruptedException, IOException
	{
		WebElement image = driver.findElement(locator);
		HttpClient client = HttpClientBuilder.create().build();
		int attempts = 0;
		HttpGet request = null;
		HttpResponse response = null;
		while (attempts <= 5)
		{
			Thread.sleep(100);
			try
			{
				request = new HttpGet(image.getAttribute("src"));
				response = client.execute(request);
				break;
			}
			catch (NullPointerException e)
			{
				++attempts;
			}
		}
		return response.getStatusLine().getStatusCode();
	}
}
