package com.algoworks.automationweb.Utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

public class Utils
{
	private Utils()
	{
		
	}
	
	public static String getEnvProperty(String key)
	{
		String v = System.getenv(key);
		if(StringUtils.isNotBlank(v))
			return v;
		else
			return null;
	}
	
	public static String convertTimestamptoText()
	{
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
		timeStamp = timeStamp.replace(".", "");
		return timeStamp;
	}
	
	public static boolean isTextNotBlankandEmpty(String actualMessage)
	{
		return (StringUtils.isNotBlank(actualMessage) && StringUtils.isNotEmpty(actualMessage));
	}
	
	public static boolean isEmptyOrBlank(String text)
	{
		return (StringUtils.isEmpty(text) || StringUtils.isBlank(text));
	}
}
