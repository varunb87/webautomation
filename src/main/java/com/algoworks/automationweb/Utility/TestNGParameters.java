package com.algoworks.automationweb.Utility;

import org.testng.ITestContext;

public class TestNGParameters
{
	private ITestContext context;

	public TestNGParameters(ITestContext context)
	{
		this.context = context;
	}

	public String getBrowsername()
	{
		return context.getCurrentXmlTest().getParameter("browser");
	}

	public String getComputername()
	{
		return context.getCurrentXmlTest().getParameter("computername");
	}

	public String getHubIPAddress()
	{
		return context.getCurrentXmlTest().getParameter("hubIP");
	}

	public String getSeleniumServerAddress()
	{
		return ("http://" + context.getCurrentXmlTest().getParameter("hubIP") + ":4444/wd/hub");
	}

	public String getPlatformname()
	{
		return context.getCurrentXmlTest().getParameter("platform");
	}

	public String getTestcaseFilepath()
	{
		return context.getCurrentXmlTest().getParameter("excelFilepath");
	}

	public String getTestcaseFilename()
	{
		return context.getCurrentXmlTest().getParameter("excelFile");
	}

	public String getBaseurl() 
	{
		return context.getCurrentXmlTest().getParameter("baseurl");
	}
}
