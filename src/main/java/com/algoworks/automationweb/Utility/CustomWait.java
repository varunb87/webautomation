package com.algoworks.automationweb.Utility;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CustomWait
{
	private FluentWait<RemoteWebDriver> fluentWait = null;
	private RemoteWebDriver driver = null;
	
	public CustomWait(RemoteWebDriver driver)
	{
		this.driver = driver;
	}
	
	public void removeImplicitWait()
	{
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	public void addImplicitWait(int seconds)
	{
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);
	}

	public void explicitWait(By locator)
	{
		fluentWait = new FluentWait<>(driver);
		fluentWait.pollingEvery(1, TimeUnit.SECONDS);
		fluentWait.withTimeout(30, TimeUnit.SECONDS);
		fluentWait.ignoring(StaleElementReferenceException.class, NoSuchElementException.class);

		Function<RemoteWebDriver, WebElement> func = new Function<RemoteWebDriver, WebElement>()
		{
			public WebElement apply(RemoteWebDriver driver)
			{
				return driver.findElement(locator);
			}
		};
		fluentWait.until(func);
	}
	
	public void explicitWait(By locator, String expectedData)
	{
		fluentWait = new FluentWait<>(driver);
		fluentWait.pollingEvery(1, TimeUnit.SECONDS);
		fluentWait.withTimeout(30, TimeUnit.SECONDS);
		fluentWait.ignoring(StaleElementReferenceException.class, NoSuchElementException.class);

		Function<RemoteWebDriver, Boolean> func = new Function<RemoteWebDriver, Boolean>()
		{
			public Boolean apply(RemoteWebDriver driver)
			{
				return (driver.findElement(locator).getText().contains(expectedData));
			}
		};
		fluentWait.until(func);
	}
	
	public void waitForElement(WebElement element)
	{
		WebDriverWait wait = (WebDriverWait) new WebDriverWait(driver, 30).ignoring(StaleElementReferenceException.class);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public void waitForElement(By locator, int timeout)
	{
		WebDriverWait wait = (WebDriverWait) new WebDriverWait(driver, timeout).ignoring(StaleElementReferenceException.class);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}
	
	public void waitForElementInvisibility(By locator)
	{
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}
	
	public boolean waitForElementToHide(By locator) throws InterruptedException
	{
		boolean isHidden = false;
		int attempts = 0;
		while (attempts < 30 && !isHidden)  // If its false then that means the element is hidden
		{
			Thread.sleep(100);
			try
			{
				driver.findElement(locator).isDisplayed();
			}
			catch (StaleElementReferenceException | ElementNotVisibleException | TimeoutException e)
			{
				isHidden = true;
			} 
		}
		return isHidden;
	}
	
	public void waitForText(By locator, String text)
	{
		explicitWait(locator);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.textToBePresentInElementLocated(locator, text));
	}
}
