package com.algoworks.automationweb.Utility;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.Assert;

public class ReadExcel
{
	@SuppressWarnings("deprecation")
	public Object[][] returnArray(String excelFileName, String sheetname) throws InvalidFormatException, IOException
	{
		Object[][] arr = null;
		File file = new File(excelFileName);
		Workbook wb = WorkbookFactory.create(file);
		Sheet sheet = wb.getSheet(sheetname);
		wb.close();

		int rownum = sheet.getPhysicalNumberOfRows();
		if (rownum == 0 || rownum == 1)
		{
			Assert.fail("Please ensure that Excelsheet has atleast 2 rows: header row + data row");
		}

		arr = new String[rownum][];
		for (int r = 0; r < rownum; r++)
		{
			Row row = sheet.getRow(r); // if row not defined then return null
			if (row == null) // Skip the row if its entirely null
				continue;
			int col = row.getPhysicalNumberOfCells();
			arr[r] = new String[col];
			for (int c = 0; c < col; c++)
			{
				Cell cell = row.getCell(c);
				if (cell != null)
				{
					switch (cell.getCellType())
					{
						case Cell.CELL_TYPE_NUMERIC:
						{
							arr[r][c] = getNumericCellValue(cell);
							break;
						}

						case Cell.CELL_TYPE_STRING:
						{
							arr[r][c] = cell.getStringCellValue();
							break;
						}

						case Cell.CELL_TYPE_BLANK:
						{
							arr[r][c] = " ";
							break;
						}

						case Cell.CELL_TYPE_FORMULA:
						{
							switch (cell.getCachedFormulaResultType())
							{
								case Cell.CELL_TYPE_STRING:
									arr[r][c] = cell.getRichStringCellValue().getString();
									break;
								case Cell.CELL_TYPE_NUMERIC:
									arr[r][c] = getNumericCellValue(cell);
									break;
							}
							break;
						}
					}
				}
			}
		}
		return arr;
	}

	public String getNumericCellValue(Cell cell)
	{
		double value = cell.getNumericCellValue();
		NumberFormat f = NumberFormat.getInstance();
		f.setGroupingUsed(false);
		String finalValue = f.format(value);
		return finalValue;
	}
}
