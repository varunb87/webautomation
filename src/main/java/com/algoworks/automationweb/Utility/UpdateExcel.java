package com.algoworks.automationweb.Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.algoworks.automationweb.Drivers.MainDriver;

public class UpdateExcel
{
	private String excelFilePath = MainDriver.getSheetPath();
	private String sheetName = MainDriver.getTestCaseName();

	@SuppressWarnings("deprecation")
	public void updateCell(int row, int column)
	{
		try
		{
			FileInputStream excelFileToRead = new FileInputStream(excelFilePath);
			XSSFWorkbook wb = new XSSFWorkbook(excelFileToRead);
			XSSFSheet sheet = wb.getSheet(sheetName);
			XSSFCell cell = sheet.getRow(row).getCell(column);

			if (cell.getCellType() == XSSFCell.CELL_TYPE_FORMULA)
			{
				if (cell.getCachedFormulaResultType() == Cell.CELL_TYPE_STRING)
				{
					String currentCellvalue = cell.getStringCellValue();
					incrementCellValue(cell, currentCellvalue);
				}

				else if (cell.getCachedFormulaResultType() == Cell.CELL_TYPE_NUMERIC)
				{
					ReadExcel read = new ReadExcel();
					String currentCellvalue = read.getNumericCellValue(cell);
					incrementCellValue(cell, currentCellvalue);
				}
			}
			excelFileToRead.close();
			FileOutputStream outFile = new FileOutputStream(new File(excelFilePath));
			wb.write(outFile);
			outFile.close();
			wb.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			System.out.println(excelFilePath + " not found!");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private void incrementCellValue(XSSFCell cell, String currentCellvalue)
	{
		int numericValue = Integer.parseInt(currentCellvalue);
		++numericValue;
		cell.setCellValue(numericValue);
	}
}
