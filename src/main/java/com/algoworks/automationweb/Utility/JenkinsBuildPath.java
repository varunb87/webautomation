package com.algoworks.automationweb.Utility;

import java.io.File;
import javax.management.InvalidAttributeValueException;
import org.testng.annotations.Listeners;
import com.algoworks.automationweb.ListenerInfo.AutomationListener;

@Listeners({ AutomationListener.class })
public class JenkinsBuildPath
{
	private static final String USERDIRECTORY = System.getProperty("user.dir");

	private JenkinsBuildPath()
	{

	}

	public static String getBuildPath()
	{
		String buildPath = null;
		buildPath = USERDIRECTORY + System.getProperty("file.separator") + System.getProperty("buildTag");
		File file = new File(buildPath);
		if (!file.exists())
		{
			file.mkdirs();
		}
		return buildPath;
	}

	public static String getSourcePath(TestNGParameters parameters) throws InvalidAttributeValueException
	{
		String path = "";
		/* If job is supposed to be run on a QA's computer then excel file path needs to be provided */
		if (Utils.isTextNotBlankandEmpty(parameters.getTestcaseFilepath()))
		{
			path = parameters.getTestcaseFilepath();
			if (!path.endsWith(File.separator))
			{
				path = path + File.separator;
			}
		}

		/* If the jobtype = regression then it means job will run for regression on the SERVER */
		else
		{
			path = USERDIRECTORY + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator;
		}
		return path.trim();
	}

	public static String getProjectPath()
	{
		return USERDIRECTORY;
	}
}
