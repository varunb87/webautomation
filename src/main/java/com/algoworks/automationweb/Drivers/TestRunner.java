package com.algoworks.automationweb.Drivers;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.ElementNotSelectableException;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.RemoteWebDriver;
import com.algoworks.automationweb.Assertion.CustomAssert;
import com.algoworks.automationweb.Errors.ErrorReporting;
import com.algoworks.automationweb.Keyword.PerformAction;
import com.algoworks.automationweb.Utility.FindSpreadsheet;
import com.algoworks.automationweb.Utility.TestNGParameters;
import com.algoworks.automationweb.Utility.Utils;

public class TestRunner
{
	private String[][] testStep;
	private String action, element, category, expectedValue, failMessage;
	private int row;
	private PerformAction perform = null;
	private ArrayList<String> dataErrors = new ArrayList<>();
	private ErrorReporting errors = null;
	private RemoteWebDriver driver = null;
	private FindSpreadsheet sheet = null;
	private static final Logger log = LogManager.getLogger(TestRunner.class.getName());
	private Throwable e = null;
	private int column = 4;

	public void runTest(String fileName, String testCaseSheet, TestNGParameters parameters, CustomAssert customAssert) throws IOException
	{
		perform = new PerformAction(parameters, customAssert);
		driver = BaseDriver.getDriver();
		errors = new ErrorReporting(driver);
		sheet = new FindSpreadsheet();
		boolean isValidExcel = false;
		try
		{
			Object[][] tempcast;
			tempcast = sheet.getArray(fileName, testCaseSheet);  /* If it fails here then isValidExcel = false will be used for catching exception */
			testStep = sheet.getStringArrayfromObject(tempcast);
			isValidExcel = true;
			for (column = 4; Utils.isTextNotBlankandEmpty(testStep[0][column].trim()); column++)
			{
				for (row = 1; row < testStep.length; row++)
				{
					expectedValue = testStep[row][column];
					// To skip a test step, simply type Skip in the Value cell
					if (("skip").equalsIgnoreCase(expectedValue) || ("skip").contains(expectedValue))
					{
						continue;
					}
					else
					{
						action = testStep[row][1];
						element = testStep[row][2];
						category = testStep[row][3];
						try
						{
							perform.perform(action, element, category, expectedValue);
						}
						catch (NoSuchElementException e)
						{
							detectFailureMsg("NoSuchElementException! Please recheck your locator in the Excel sheet or re-run the test.");
							this.e = e;
							errorLogging(testCaseSheet);
						}
						catch (TimeoutException e)
						{
							detectFailureMsg("TimeoutException! Operation timed out & " + element + " was not found!");
							this.e = e;
							errorLogging(testCaseSheet);
						}
						catch (ElementNotVisibleException e)
						{
							detectFailureMsg("ElementNotVisibleException! The element was not visible on the page.");
							this.e = e;
							errorLogging(testCaseSheet);
						}
						catch (ElementNotSelectableException e)
						{
							detectFailureMsg("ElementNotSelectableException! " + element + " cannot be selected. Please recheck your locator in the Excel sheet & re-run the test.");
							this.e = e;
							errorLogging(testCaseSheet);
						}
						catch (NullPointerException e)
						{
							// throw new SkipException("Skipping test.");
						}
						catch (AssertionError | Exception e)
						{
							failMessage = perform.getFailMessage(); // this will contain some assertion failure message.
							this.e = e;
							errorLogging(testCaseSheet);
						}
					}
				}
			}
		}

		catch (ArrayIndexOutOfBoundsException | NullPointerException e)
		{
			if (!isValidExcel)
			{
				log.fatal("Excel file was not read properly!", e);
				throw e;
			}
			else if (column == 4)
			{
				log.fatal("ArrayIndexOutOfBoundsException! Required value column does not exist.", e);
				throw e;
			}
		}
		catch (Exception e)
		{
			log.error("Unknown exception!");
			e.printStackTrace();
		}
	}

	private void errorLogging(String testCaseSheet) throws IOException
	{
		dataErrors.add(action);
		dataErrors.add(element);
		dataErrors.add(expectedValue);
		dataErrors.add(Integer.toString(row));
		errors.logErrors(e, testCaseSheet, dataErrors, failMessage);
	}

	private void detectFailureMsg(String msg)
	{
		failMessage = perform.getFailMessage();
		if (StringUtils.isEmpty(failMessage) && StringUtils.isBlank(failMessage))
		{
			failMessage = msg;
		}
	}
}
