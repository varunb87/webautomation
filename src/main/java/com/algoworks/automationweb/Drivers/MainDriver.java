package com.algoworks.automationweb.Drivers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.algoworks.automationweb.Assertion.CustomAssert;
import com.algoworks.automationweb.ListenerInfo.AutomationListener;
import com.algoworks.automationweb.Reporting.ExtentTesting;
import com.algoworks.automationweb.Utility.ArrayActions;
import com.algoworks.automationweb.Utility.FindSpreadsheet;
import com.algoworks.automationweb.Utility.JenkinsBuildPath;
import com.algoworks.automationweb.Utility.TestNGParameters;
import com.google.common.base.Throwables;

@Listeners({ AutomationListener.class })
public class MainDriver
{
	private static ThreadLocal<String> absoluteTestSheetPath = new ThreadLocal<>();
	private static ThreadLocal<String> threadTestCaseName = new ThreadLocal<>();
	private static final Logger logger = LogManager.getLogger(MainDriver.class.getName());
	private Object[][] testData;
	private static ThreadLocal<ExtentTesting> extentThread = new ThreadLocal<>();
	private TestRunner single = null;
	private CustomAssert customAssert = null;
	private static ThreadLocal<CustomAssert> assertThread = new ThreadLocal<>();
	private boolean allTests = false;
	private boolean sheetExists = false;
	private static final String SKIPMSG = "Failure! Driver not initialized. Skipping test!";

	@BeforeClass
	public void setup(final ITestContext testContext)
	{
		if (BaseDriver.getDriver() != null)
		{
			try
			{
				ExtentTesting et = new ExtentTesting();
				TestNGParameters parameters = new TestNGParameters(testContext);
				customAssert = new CustomAssert();
				single = new TestRunner();
				String report = BaseDriver.getTestName() + "_" + BaseDriver.getBrowser() + "_" + System.getProperty("buildNumber") + ".html";
				String excelFileName = parameters.getTestcaseFilename();

				/* Now the above values will be used to get the report path + name */
				String finalReportPath = new File(JenkinsBuildPath.getBuildPath(), report).getAbsolutePath();

				/* Send the HTML report path to the reporter */
				String browser = testContext.getCurrentXmlTest().getParameter("browser");
				browser = browser.substring(0, 1).toUpperCase() + browser.substring(1);
				et.createReports(finalReportPath, browser);
				extentThread.set(et);

				/* Now we read the excelsheet */
				String absoluteSheetPath = JenkinsBuildPath.getSourcePath(parameters) + excelFileName;
				absoluteSheetPath = new File(absoluteSheetPath).getAbsolutePath();
				absoluteTestSheetPath.set(absoluteSheetPath);

				// Get the regression sheet name
				String regression = testContext.getCurrentXmlTest().getParameter("regressionSheet");
				FindSpreadsheet find = new FindSpreadsheet();
				testData = find.getArray(absoluteTestSheetPath.get(), regression);
				sheetExists = true;
			}
			catch (FileNotFoundException e)
			{
				logger.fatal(e);
				Assert.fail(Throwables.getStackTraceAsString(e));
			}
			catch (Exception e)
			{
				logger.fatal(e + " was encountered!", e);
				Assert.fail(Throwables.getStackTraceAsString(e));
			}
		}
		else
		{
			throw new SkipException(SKIPMSG);
		}
	}

	@DataProvider(name = "regression")
	public Object[][] createDP()
	{
		List<List<Object>> temp = new ArrayList<>();
		Object[][] dp = null;
		if (testData != null)
		{
			int index = -1;
			for (int i = 1; i < testData.length; i++)
			{
				if (!("round".equalsIgnoreCase(testData[i][0].toString())) && ("y".equalsIgnoreCase(testData[i][2].toString())))
				{
					++index;
					temp.add(index, new ArrayList<>());
					for (int j = 0; j < testData[i].length; j++)
					{
						temp.get(index).add(testData[i][j]);
					}
				}
			}
			ArrayActions actions = new ArrayActions();
			dp = actions.getArrayfromList(temp);
		}
		else
		{
			dp = new Object[0][0];
		}
		return dp;
	}

	@Test(dataProvider = "regression")
	public void mainTest(String testCase, String testDescription, String execution, final ITestContext context) throws IOException
	{
		TestNGParameters parameters = new TestNGParameters(context);
		assertThread.set(customAssert);
		allTests = true;
		threadTestCaseName.set(testCase);
		System.out.println();
		logger.info("Currently executing " + "\"" + testCase + "\"");
		getExtentTestObject().createTest(testCase, testDescription);
		single.runTest(absoluteTestSheetPath.get(), testCase, parameters, customAssert);
		customAssert.displayVerificationErrors();
	}

	/* We had to use AfterClass instead of AfterMethod because AfterMethod was triggering after every dataprovider value was
	 * used by Test method. Because of this it kept printing "All tests completed in ChromeTest suite." text after every
	 * dataprovider value */

	@AfterClass
	public void runAfterTest()
	{
		if (BaseDriver.getDriver() != null)
		{
			if (!allTests && sheetExists) /* Display the below error only if sheet exists but no test case is enabled */
				logger.error("You have not enabled any test case in the regression sheet. Please set any test case to 'Yes'.");
			else
			{
				getExtentTestObject().endTests();
				removeThreads();
			}
		}
		else
		{
			throw new SkipException(SKIPMSG);
		}
	}

	public static String getSheetPath()
	{
		return absoluteTestSheetPath.get();
	}

	public static String getTestCaseName()
	{
		return threadTestCaseName.get();
	}

	public static CustomAssert getAssertion()
	{
		return assertThread.get();
	}

	public static ExtentTesting getExtentTestObject()
	{
		return extentThread.get();
	}

	private void removeThreads()
	{
		absoluteTestSheetPath.remove();
		assertThread.remove();
		extentThread.remove();
		threadTestCaseName.remove();
	}
}
