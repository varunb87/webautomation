package com.algoworks.automationweb.Drivers;

import org.testng.annotations.BeforeTest;
import com.algoworks.automationweb.CapabilitiesInfo.CreateDriver;
import com.algoworks.automationweb.Utility.TestNGParameters;
import com.google.common.base.Throwables;

import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.grid.common.exception.GridException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;

public class BaseDriver
{
	private static ThreadLocal<String> testName = new ThreadLocal<>();
	private static ThreadLocal<RemoteWebDriver> threadDriver = new ThreadLocal<>();
	private static ThreadLocal<String> browserThread = new ThreadLocal<>();
	private static final Logger logger = LogManager.getLogger(BaseDriver.class);

	@BeforeTest
	public void beforeTest(final ITestContext testContext) throws IOException
	{
		TestNGParameters parameters = new TestNGParameters(testContext);
		String browser = parameters.getBrowsername();
		browserThread.set(browser);
		testName.set(testContext.getName());
		CreateDriver create = new CreateDriver();
		RemoteWebDriver driver = null;
		try
		{
			driver = create.initializeDriver(parameters);
			threadDriver.set(driver);
			getDriver().manage().window().maximize();
			/*if ((platform.equalsIgnoreCase("mac") || platform.equalsIgnoreCase("linux") || platform.equalsIgnoreCase("ubuntu")) && (browser.equalsIgnoreCase("chrome")))
			{
				getDriver().manage().window().fullscreen();
			}*/
		}

		catch (GridException | WebDriverException e)
		{
			if (e.toString().contains("Error forwarding the new session"))
			{
				logger.fatal("Failure! Please ensure that your hub is running & the nodes are connected to it!");
			}
			else if (e.toString().contains("Unable to create new service: ChromeDriverService"))
			{
				logger.fatal("There is a problem launching Chrome. Please ensure you have the updated version of Chromedriver binary in your path and that you are running the updated Selenium library." + System.lineSeparator() + e);
			}
			else
			{
				logger.fatal(e);
			}
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
		catch (Exception e)
		{
			logger.fatal("Unknown exception", e);
			Assert.fail(Throwables.getStackTraceAsString(e));
		}
	}

	@AfterTest
	public void afterTest()
	{
		if (getDriver() != null)
		{
			getDriver().quit();
			removeThread();
		}
		else
			throw new SkipException("");
	}

	public static RemoteWebDriver getDriver()
	{
		return threadDriver.get();
	}

	private void removeThread()
	{
		threadDriver.remove();
		testName.remove();
	}

	public static String getTestName()
	{
		return testName.get();
	}

	public static String getBrowser()
	{
		return browserThread.get();
	}
}
