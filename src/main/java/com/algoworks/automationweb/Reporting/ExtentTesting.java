package com.algoworks.automationweb.Reporting;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentTesting
{
	private static ThreadLocal<ExtentTest> testThread = new ThreadLocal<>();
	private ExtentReports reporter = null;

	public void createReports(String finalReportPath, String browser) throws UnknownHostException
	{
		// initialize the HtmlReporter
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(finalReportPath);
		reporter = new ExtentReports();

		// attach only HtmlReporter
		reporter.attachReporter(htmlReporter);
		htmlReporter.setAppendExisting(true);
		reporter.setSystemInfo("Operating System", System.getProperty("os.name"));
		reporter.setSystemInfo("Computer Name", InetAddress.getLocalHost().getHostName());
		reporter.setSystemInfo("Java Version",
				System.getProperty("java.runtime.name") + " " + System.getProperty("java.runtime.version"));
		reporter.setSystemInfo("Browser", browser);
	}

	public void createTest(String currentTestName, String testDescription)
	{
		ExtentTest extentTest = reporter.createTest(currentTestName, testDescription);
		testThread.set(extentTest);
	}

	public void displayWarning(String warnMessage)
	{
		testThread.get().log(Status.WARNING, warnMessage);
	}
	
	public void markFatal(String fatalMessage, String screenshotPath) throws IOException
	{
		testThread.get().log(Status.FATAL, fatalMessage).addScreenCaptureFromPath(screenshotPath);
	}

	public void passTest(String passMessage)
	{
		testThread.get().log(Status.PASS, passMessage);
	}

	public void failTest(String failMessage, String screenshotPath) throws IOException
	{
		testThread.get().log(Status.FAIL, failMessage).addScreenCaptureFromPath(screenshotPath);
	}

	public void errorTest(String failMessage, String screenshotPath) throws IOException
	{
		testThread.get().log(Status.ERROR, failMessage).addScreenCaptureFromPath(screenshotPath);
	}

	public void logInfo(String failMessage)
	{
		testThread.get().log(Status.INFO, failMessage);
	}

	public void endTests()
	{
		reporter.flush();
	}
}
