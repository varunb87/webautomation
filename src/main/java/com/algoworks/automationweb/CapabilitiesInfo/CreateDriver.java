package com.algoworks.automationweb.CapabilitiesInfo;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariOptions;

import com.algoworks.automationweb.Utility.JenkinsBuildPath;
import com.algoworks.automationweb.Utility.TestNGParameters;

public class CreateDriver
{
	private DesiredCapabilities capabilities = null;
	public RemoteWebDriver initializeDriver(TestNGParameters parameters)
			throws IOException
	{
		String platform = parameters.getPlatformname();
		switch (parameters.getBrowsername().toLowerCase())
		{
			case "firefox":
				capabilities = DesiredCapabilities.firefox();
				getPlatformCapabilities(platform);
				
				/* Skip the following code for now if the OS is Mac */
				if (!(SystemUtils.IS_OS_MAC_OSX || SystemUtils.IS_OS_LINUX) && platform.contains("WIN"))
				{
					System.setProperty("webdriver.gecko.driver", JenkinsBuildPath.getProjectPath() + "//Executables//geckodriver.exe");
				}
				FirefoxProfile profile = new FirefoxProfile();
				profile.setPreference("security.insecure_password.ui.enabled", false);
				profile.setPreference("security.insecure_field_warning.contextual.enabled", false);
				capabilities.setCapability(FirefoxDriver.PROFILE, profile);
				break;

			case "chrome":
				capabilities = DesiredCapabilities.chrome();
				getPlatformCapabilities(platform);
				
				/* For Windows only*/
				if (!(SystemUtils.IS_OS_MAC_OSX || SystemUtils.IS_OS_LINUX) && platform.toUpperCase().contains("WIN"))
				{
					ChromeOptions options = new ChromeOptions();
					System.setProperty("webdriver.chrome.driver", JenkinsBuildPath.getProjectPath() + File.separator + "Executables" + File.separator + "chromedriver.exe");
					options.addArguments("--start-maximized");
					options.addArguments("--disable-web-security");
					Map<String, Object> prefs = new HashMap<>();
					prefs.put("credentials_enable_service", false);
					prefs.put("profile.password_manager_enabled", false);
					options.setExperimentalOption("prefs", prefs);
					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
				}
				LoggingPreferences loggingprefs = new LoggingPreferences();
		        loggingprefs.enable(LogType.BROWSER, Level.ALL);
		        capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingprefs);
				break;

			case "iexplorer":
				capabilities = DesiredCapabilities.internetExplorer();
				break;
				
			case "safari":
				capabilities = DesiredCapabilities.safari();
				getPlatformCapabilities(platform);
				SafariOptions safariOptions = new SafariOptions();
				//safariOptions.useCleanSession(true);
				safariOptions.setUseTechnologyPreview(true);
				capabilities.setCapability(SafariOptions.CAPABILITY, safariOptions);
				break;
				
			case "edge":
				capabilities = DesiredCapabilities.edge();
				break;
			
			default:
				throw new WebDriverException("this is an invalid browser!");
		}
		capabilities.setCapability("computername", "");
		return new RemoteWebDriver(new URL(parameters.getSeleniumServerAddress()), capabilities);
	}

	private void getPlatformCapabilities(String platform)
	{
		if (("WIN10").equalsIgnoreCase(platform))
			capabilities.setPlatform(Platform.WIN10);
		else if (("WIN8").equalsIgnoreCase(platform))
			capabilities.setPlatform(Platform.WIN8);
		else if (("WIN8.1").equalsIgnoreCase(platform))
			capabilities.setPlatform(Platform.WIN8_1);
		else if (("LINUX").equalsIgnoreCase(platform))
			capabilities.setPlatform(Platform.LINUX);
		else if (("MAC").equalsIgnoreCase(platform))
			capabilities.setPlatform(Platform.MAC);
		else if (("YOSEMITE").equalsIgnoreCase(platform))
			capabilities.setPlatform(Platform.YOSEMITE);
		else if (("EL CAPITAN").equalsIgnoreCase(platform))
			capabilities.setPlatform(Platform.EL_CAPITAN);
		else if (("SIERRA").equalsIgnoreCase(platform))
			capabilities.setPlatform(Platform.SIERRA);
		else
			capabilities.setPlatform(Platform.ANY);
	}
}
